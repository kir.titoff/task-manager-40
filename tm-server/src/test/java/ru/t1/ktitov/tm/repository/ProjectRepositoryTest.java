package ru.t1.ktitov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktitov.tm.api.repository.IProjectRepository;
import ru.t1.ktitov.tm.api.service.IConnectionService;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.service.ConnectionService;
import ru.t1.ktitov.tm.service.PropertyService;

import java.util.List;

import static ru.t1.ktitov.tm.constant.ProjectTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private void compareProjects(@NotNull final Project project1, @NotNull final Project project2) {
        Assert.assertEquals(project1.getId(), project2.getId());
        Assert.assertEquals(project1.getName(), project2.getName());
        Assert.assertEquals(project1.getDescription(), project2.getDescription());
        Assert.assertEquals(project1.getStatus(), project2.getStatus());
        Assert.assertEquals(project1.getUserId(), project2.getUserId());
        Assert.assertEquals(project1.getCreated(), project2.getCreated());
    }

    private void compareProjects(
            @NotNull final List<Project> projectList1,
            @NotNull final List<Project> projectList2) {
        Assert.assertEquals(projectList1.size(), projectList2.size());
        for (int i = 0; i < projectList1.size(); i++) {
            compareProjects(projectList1.get(i), projectList2.get(i));
        }
    }

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.clearProjectsByUser(USER1.getId());
            repository.clearProjectsByUser(USER2.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            Assert.assertTrue(repository.findAllProjects().isEmpty());
            repository.addProject(USER1_PROJECT1);
            compareProjects(USER1_PROJECT1, repository.findAllProjects().get(0));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.addProject(USER1_PROJECT1);
            compareProjects(USER1_PROJECT1, repository.findAllProjects().get(0));
            repository.clearProjects();
            Assert.assertTrue(repository.findAllProjects().isEmpty());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.addProject(USER1_PROJECT1);
            repository.addProject(USER1_PROJECT2);
            repository.addProject(USER1_PROJECT3);
            repository.clearProjectsByUser(USER2.getId());
            Assert.assertFalse(repository.findAllProjects().isEmpty());
            repository.clearProjectsByUser(USER1.getId());
            Assert.assertTrue(repository.findAllProjects().isEmpty());
            repository.addProject(USER1_PROJECT1);
            repository.clearProjectsByUser(USER2.getId());
            compareProjects(USER1_PROJECT1, repository.findAllProjects().get(0));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.addProject(USER1_PROJECT1);
            repository.addProject(USER1_PROJECT2);
            repository.addProject(USER1_PROJECT3);
            repository.addProject(USER2_PROJECT1);
            Assert.assertEquals(4, repository.findAllProjects().size());
            Assert.assertEquals(3, repository.findAllProjectsByUser(USER1.getId()).size());
            Assert.assertEquals(1, repository.findAllProjectsByUser(USER2.getId()).size());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.addProject(USER1_PROJECT1);
            repository.addProject(USER1_PROJECT2);
            repository.addProject(USER1_PROJECT3);
            compareProjects(USER1_PROJECT1, repository.findProjectById(USER1_PROJECT1.getId()));
            Assert.assertNull(repository.findProjectByIdByUser(USER2.getId(), USER1_PROJECT1.getId()));
            compareProjects(USER1_PROJECT1, repository.findProjectByIdByUser(USER1.getId(), USER1_PROJECT1.getId()));
            Assert.assertNotNull(repository.findProjectById(USER1_PROJECT1.getId()));
            Assert.assertNull(repository.findProjectByIdByUser(USER2.getId(), USER1_PROJECT1.getId()));
            Assert.assertNotNull(repository.findProjectByIdByUser(USER1.getId(), USER1_PROJECT1.getId()));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void remove() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.addProject(USER1_PROJECT1);
            repository.addProject(USER1_PROJECT2);
            repository.addProject(USER1_PROJECT3);
            repository.removeProject(USER1_PROJECT1);
            Assert.assertEquals(2, repository.getProjectsSize());
            repository.removeProjectById(USER1_PROJECT2.getId());
            Assert.assertEquals(1, repository.getProjectsSize());
            compareProjects(USER1_PROJECT3, repository.findAllProjects().get(0));
            repository.clearProjects();
            repository.addProject(USER1_PROJECT1);
            repository.addProject(USER1_PROJECT2);
            repository.addProject(USER1_PROJECT3);
            Assert.assertEquals(3, repository.getProjectsSize());
            repository.removeProjectByIdByUser(USER2.getId(), USER1_PROJECT1.getId());
            Assert.assertEquals(3, repository.getProjectsSize());
            repository.removeProjectByIdByUser(USER1.getId(), USER1_PROJECT1.getId());
            repository.removeProjectByIdByUser(USER1.getId(), USER1_PROJECT2.getId());
            compareProjects(USER1_PROJECT3, repository.findAllProjects().get(0));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
