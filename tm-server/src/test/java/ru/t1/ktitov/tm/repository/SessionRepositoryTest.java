package ru.t1.ktitov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktitov.tm.api.repository.ISessionRepository;
import ru.t1.ktitov.tm.api.service.IConnectionService;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.model.Session;
import ru.t1.ktitov.tm.service.ConnectionService;
import ru.t1.ktitov.tm.service.PropertyService;

import java.util.List;

import static ru.t1.ktitov.tm.constant.SessionTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private void compareSessions(@NotNull final Session session1, @NotNull final Session session2) {
        Assert.assertEquals(session1.getId(), session2.getId());
        Assert.assertEquals(session1.getUserId(), session2.getUserId());
        Assert.assertEquals(session1.getRole(), session2.getRole());
    }

    private void compareSessions(
            @NotNull final List<Session> sessionList1,
            @NotNull final List<Session> sessionList2) {
        Assert.assertEquals(sessionList1.size(), sessionList2.size());
        for (int i = 0; i < sessionList1.size(); i++) {
            compareSessions(sessionList1.get(i), sessionList2.get(i));
        }
    }

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.clearSessionsByUser(USER1_SESSION1.getId());
            repository.clearSessionsByUser(USER1_SESSION2.getId());
            repository.clearSessionsByUser(USER1_SESSION3.getId());
            repository.clearSessionsByUser(USER2_SESSION1.getId());
            repository.clearSessionsByUser(USER2_SESSION2.getId());
            repository.clearSessionsByUser(USER2_SESSION3.getId());
            repository.clearSessionsByUser(ADMIN1_SESSION1.getId());
            repository.clearSessionsByUser(ADMIN1_SESSION2.getId());
            repository.clearSessionsByUser(ADMIN1_SESSION3.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            Assert.assertTrue(repository.findAllSessions().isEmpty());
            repository.addSession(USER1_SESSION1);
            compareSessions(USER1_SESSION1, repository.findAllSessions().get(0));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void addList() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            Assert.assertTrue(repository.findAllSessions().isEmpty());
            repository.addSession(USER1_SESSION1);
            repository.addSession(USER1_SESSION2);
            repository.addSession(USER1_SESSION3);
            Assert.assertEquals(3, repository.getSessionsSize());
            compareSessions(USER1_SESSION_LIST, repository.findAllSessions());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.addSession(USER1_SESSION1);
            compareSessions(USER1_SESSION1, repository.findAllSessions().get(0));
            repository.clearSessions();
            Assert.assertTrue(repository.findAllSessions().isEmpty());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.addSession(USER1_SESSION1);
            repository.addSession(USER1_SESSION2);
            repository.addSession(USER1_SESSION3);
            compareSessions(USER1_SESSION_LIST, repository.findAllSessions());
            repository.clearSessionsByUser(USER2.getId());
            Assert.assertFalse(repository.findAllSessions().isEmpty());
            repository.clearSessionsByUser(USER1.getId());
            Assert.assertTrue(repository.findAllSessions().isEmpty());
            repository.addSession(USER1_SESSION1);
            repository.addSession(USER2_SESSION1);
            repository.clearSessionsByUser(USER1.getId());
            compareSessions(USER2_SESSION1, repository.findAllSessions().get(0));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAllSessions() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.addSession(USER1_SESSION1);
            repository.addSession(USER1_SESSION2);
            repository.addSession(USER1_SESSION3);
            repository.addSession(USER2_SESSION1);
            repository.addSession(USER2_SESSION2);
            repository.addSession(USER2_SESSION3);
            Assert.assertEquals(6, repository.getSessionsSize());
            compareSessions(USER1_SESSION_LIST, repository.findAllSessionsByUser(USER1.getId()));
            compareSessions(USER2_SESSION_LIST, repository.findAllSessionsByUser(USER2.getId()));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.addSession(USER1_SESSION1);
            repository.addSession(USER1_SESSION2);
            repository.addSession(USER1_SESSION3);
            compareSessions(USER1_SESSION1, repository.findSessionById(USER1_SESSION1.getId()));
            Assert.assertNull(repository.findSessionByIdByUser(USER2.getId(), USER1_SESSION1.getId()));
            compareSessions(USER1_SESSION1, repository.findSessionByIdByUser(USER1.getId(), USER1_SESSION1.getId()));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void remove() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.addSession(USER1_SESSION1);
            repository.addSession(USER1_SESSION2);
            repository.addSession(USER1_SESSION3);
            repository.removeSession(USER1_SESSION1);
            Assert.assertEquals(2, repository.getSessionsSize());
            repository.removeSessionById(USER1_SESSION2.getId());
            Assert.assertEquals(1, repository.getSessionsSize());
            compareSessions(USER1_SESSION3, repository.findAllSessions().get(0));
            repository.clearSessions();
            repository.addSession(USER1_SESSION1);
            repository.addSession(USER1_SESSION2);
            repository.addSession(USER1_SESSION3);
            Assert.assertEquals(3, repository.getSessionsSize());
            repository.removeSessionByIdByUser(USER2.getId(), USER1_SESSION1.getId());
            Assert.assertEquals(3, repository.getSessionsSize());
            repository.removeSessionByIdByUser(USER1.getId(), USER1_SESSION1.getId());
            repository.removeSessionByIdByUser(USER1.getId(), USER1_SESSION2.getId());
            compareSessions(USER1_SESSION3, repository.findAllSessions().get(0));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
